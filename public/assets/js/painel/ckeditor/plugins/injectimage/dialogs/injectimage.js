CKEDITOR.dialog.add( 'injectimageDialog', function ( editor ) {
    return {
        title: 'Inserir Imagem',
        minWidth: 400,
        minHeight: 80,
        contents: [
            {
                id: 'tab-basic',
                label: 'Selecionar Arquivo',
                elements: [
                    {
                        type: 'file',
                        id: 'userfile',
                        label: 'Selecionar Arquivo',
                        validate: CKEDITOR.dialog.validate.notEmpty('Selecione um arquivo!')
                    }
                ]
            }
        ],
        onOk: function() {
            var img = editor.document.createElement( 'img' );
            var iframe = $('.cke_dialog_body iframe');
            var form = $('form', iframe.contents());

            var oData = new FormData(form[0]);
            var oReq = new XMLHttpRequest();

            oReq.open("POST", '/assets/js/painel/ckeditor/plugins/injectimage/upload.php', true);

            oReq.onload = function(oEvent) {
                if (oReq.status == 200) {
                    retorno = JSON.parse(oReq.response);

                    if (window.console && window.console.log) console.log("Uploaded!");

                    if (!retorno.error) {
                        img.setAttribute('src', retorno.filepath);
                        editor.insertElement(img);
                    }

                } else {

                    if (window.console && window.console.log)
                        console.log("Error " + oReq.status + " occurred uploading your file.");

                }
            };

            oReq.send(oData);
        },
    };
});
