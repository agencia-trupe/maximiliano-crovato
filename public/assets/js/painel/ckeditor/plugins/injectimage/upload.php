<?php

require './vendor/autoload.php';

use Intervention\Image\ImageManagerStatic as Image;

if (isset($_FILES['userfile'])) {
    $filename = date('YmdHis').'_'.basename($_FILES['userfile']['name']);
    $error = true;

    $path = '../../../../../img/blog/'.$filename;
    $error = !move_uploaded_file($_FILES['userfile']['tmp_name'], $path);

    $image = Image::make($path)->resize(1080, null, function($constraint){
        $constraint->aspectRatio();
        $constraint->upsize();
    })->save($path, 100);

    $rsp = array(
        'error' => $error, // Used in JS
        'filename' => $filename,
        'filepath' => '/assets/img/blog/' . $filename, // Web accessible
    );

    echo json_encode($rsp);
    exit;

} else {
    echo json_encode(array('error' => 'No file'));
}
