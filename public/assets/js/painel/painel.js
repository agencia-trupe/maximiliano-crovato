$('document').ready( function(){

    if ($('#datepicker-imprensa').length) {
        $.datepicker.regional['pt-BR'] = {
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            onClose: function(dateText, inst) {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month, 1));
            },
            closeText: 'Aplicar',
            prevText: '&#x3c;Anterior',
            nextText: 'Pr&oacute;ximo&#x3e;',
            currentText: 'Hoje',
            monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
            'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
            'Jul','Ago','Set','Out','Nov','Dez'],
            dateFormat: 'mm/yy',
        };

        $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

        $("#datepicker-imprensa").datepicker();
        $('html > head').append('<style>.ui-datepicker-calendar { display: none; }</style>');

        if ($('#datepicker-imprensa').val() == '') {
            $('#datepicker-imprensa').datepicker("setDate", new Date());
        }
    }

    if ($('#datepicker-blog').length) {
        $.datepicker.regional['pt-BR'] = {
                closeText: 'Fechar',
                prevText: '&#x3c;Anterior',
                nextText: 'Pr&oacute;ximo&#x3e;',
                currentText: 'Hoje',
                monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
                'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
                'Jul','Ago','Set','Out','Nov','Dez'],
                dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
                dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 0,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };

        $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

        $("#datepicker-blog").datepicker();

        if ($('#datepicker-blog').val() == '') {
            $('#datepicker-blog').datepicker("setDate", new Date());
        }
    }

  	$('.btn-delete').click( function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
    	var result = confirm("Deseja Excluir o Registro?");
      	if(result) form.submit();
  	});

    $("table.table-sortable tbody").sortable({
        update : function () {
            serial = [];
            tabela = $('table.table-sortable').attr('data-tabela');
            $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
                serial.push(elm.id.split('_')[1])
            });
            $.post(BASE + '/painel/ajax/order', { data : serial , tabela : tabela });
        },
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle : $('.btn-move')
    }).disableSelection();

    var TEXTAREA_CONFIG = {
        perfil: {
            toolbar: [['Bold', 'Italic'], ['FontSize']]
        },

        blog: {
            extraPlugins: 'injectimage,oembed',
            allowedContent: true,
            toolbar: [['Bold', 'Italic'], ['FontSize'], ['InjectImage', 'oembed']]
        },

        clean: {
            toolbar: []
        }
    };

    $('.ckeditor').each(function (i, obj) {
        CKEDITOR.replace(obj.id, TEXTAREA_CONFIG[obj.dataset.editor]);
    });

    $('#filtro-select').on('change', function () {
        var id = $(this).val();
        if (id) {
            window.location = BASE + '/painel/blog?filtro=' + id;
        } else {
            window.location = BASE + '/painel/blog';
        }
    });

});
