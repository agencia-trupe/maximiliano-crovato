<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "The <strong>:attribute</strong> must be accepted.",
	"active_url"           => "The <strong>:attribute</strong> is not a valid URL.",
	"after"                => "The <strong>:attribute</strong> must be a date after <strong>:date</strong>.",
	"alpha"                => "The <strong>:attribute</strong> may only contain letters.",
	"alpha_dash"           => "The <strong>:attribute</strong> may only contain letters, numbers, and dashes.",
	"alpha_num"            => "The <strong>:attribute</strong> may only contain letters and numbers.",
	"array"                => "The <strong>:attribute</strong> must be an array.",
	"before"               => "The <strong>:attribute</strong> must be a date before <strong>:date</strong>.",
	"between"              => array(
		"numeric" => "The <strong>:attribute</strong> must be between <strong>:min</strong> and <strong>:max</strong>.",
		"file"    => "The <strong>:attribute</strong> must be between <strong>:min</strong> and <strong>:max</strong> kilobytes.",
		"string"  => "The <strong>:attribute</strong> must be between <strong>:min</strong> and <strong>:max</strong> characters.",
		"array"   => "The <strong>:attribute</strong> must have between <strong>:min</strong> and <strong>:max</strong> items.",
	),
	"boolean"              => "The <strong>:attribute</strong> field must be true or false.",
	"confirmed"            => "The <strong>:attribute</strong> confirmation does not match.",
	"date"                 => "The <strong>:attribute</strong> is not a valid date.",
	"date_format"          => "The <strong>:attribute</strong> does not match the format <strong>:format</strong>.",
	"different"            => "The <strong>:attribute</strong> and <strong>:other</strong> must be different.",
	"digits"               => "The <strong>:attribute</strong> must be <strong>:digits</strong> digits.",
	"digits_between"       => "The <strong>:attribute</strong> must be between <strong>:min</strong> and <strong>:max</strong> digits.",
	"email"                => "The <strong>:attribute</strong> must be a valid email address.",
	"exists"               => "The selected <strong>:attribute</strong> is invalid.",
	"image"                => "O campo <strong>:attribute</strong> deve ser um arquivo de imagem válido.",
	"in"                   => "The selected <strong>:attribute</strong> is invalid.",
	"integer"              => "The <strong>:attribute</strong> must be an integer.",
	"ip"                   => "The <strong>:attribute</strong> must be a valid IP address.",
	"max"                  => array(
		"numeric" => "The <strong>:attribute</strong> may not be greater than <strong>:max</strong>.",
		"file"    => "The <strong>:attribute</strong> may not be greater than <strong>:max</strong> kilobytes.",
		"string"  => "The <strong>:attribute</strong> may not be greater than <strong>:max</strong> characters.",
		"array"   => "The <strong>:attribute</strong> may not have more than <strong>:max</strong> items.",
	),
	"mimes"                => "The <strong>:attribute</strong> must be a file of type<strong>: :values</strong>.",
	"min"                  => array(
		"numeric" => "The <strong>:attribute</strong> must be at least <strong>:min</strong>.",
		"file"    => "The <strong>:attribute</strong> must be at least <strong>:min</strong> kilobytes.",
		"string"  => "The <strong>:attribute</strong> must be at least <strong>:min</strong> characters.",
		"array"   => "The <strong>:attribute</strong> must have at least <strong>:min</strong> items.",
	),
	"not_in"               => "The selected <strong>:attribute</strong> is invalid.",
	"numeric"              => "The <strong>:attribute</strong> must be a number.",
	"regex"                => "The <strong>:attribute</strong> format is invalid.",
	"required"             => "O campo <strong>:attribute</strong> é obrigatório.",
	"required_if"          => "The <strong>:attribute</strong> field is required when <strong>:other</strong> is <strong>:value</strong>.",
	"required_with"        => "The <strong>:attribute</strong> field is required when <strong>:values</strong> is present.",
	"required_with_all"    => "The <strong>:attribute</strong> field is required when <strong>:values</strong> is present.",
	"required_without"     => "The <strong>:attribute</strong> field is required when <strong>:values</strong> is not present.",
	"required_without_all" => "The <strong>:attribute</strong> field is required when none of <strong>:values</strong> are present.",
	"same"                 => "The <strong>:attribute</strong> and <strong>:other</strong> must match.",
	"size"                 => array(
		"numeric" => "The <strong>:attribute</strong> must be <strong>:size</strong>.",
		"file"    => "The <strong>:attribute</strong> must be <strong>:size</strong> kilobytes.",
		"string"  => "The <strong>:attribute</strong> must be <strong>:size</strong> characters.",
		"array"   => "The <strong>:attribute</strong> must contain <strong>:size</strong> items.",
	),
	"unique"               => "The <strong>:attribute</strong> has already been taken.",
	"url"                  => "The <strong>:attribute</strong> format is invalid.",
	"timezone"             => "The <strong>:attribute</strong> must be a valid zone.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'attribute-name' => array(
			'rule-name' => 'custom-message',
		),
	),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(
		'funcao' => 'função',
		'responsavel' => 'responsável',
		'titulo' => 'título',
		'email' => 'e-mail',
		'blog_categoria_id' => 'categoria'
	),

);
