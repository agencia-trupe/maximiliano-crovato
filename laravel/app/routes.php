<?php

// Front-end

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('max', ['as' => 'perfil', 'uses' => 'PerfilController@index']);

Route::get('decor/{slug?}', ['as' => 'decor', 'uses' => 'DecorController@index']);
Route::get('cenografia/{slug?}', ['as' => 'cenografia', 'uses' => 'CenografiaController@index']);
Route::get('produtos/{slug?}', ['as' => 'produtos', 'uses' => 'ProdutosController@index']);

Route::get('clipping', ['as' => 'imprensa', 'uses' => 'ImprensaController@index']);

Route::get('blog/c/{categoria}', ['as' => 'blog.cat', 'uses' => 'BlogController@filtroCategoria']);
Route::get('blog/d/{ano}/{mes?}', ['as' => 'blog.data', 'uses' => 'BlogController@filtroData']);
Route::get('blog/{slug?}', ['as' => 'blog', 'uses' => 'BlogController@index']);
Route::post('blog/comentario', ['as' => 'blog.comentario', 'uses' => 'BlogController@comentario']);

Route::get('contato', ['as' => 'contato', 'uses' => 'ContatoController@index']);
Route::post('contato', ['as' => 'contato.envio', 'uses' => 'ContatoController@envio']);


// Painel

Route::get('painel', [
    'before' => 'auth',
    'as'     => 'painel.home',
    'uses'   => 'Painel\HomeController@index'
]);

Route::get('painel/login', [
    'as'   => 'painel.login',
    'uses' => 'Painel\HomeController@login'
]);
Route::post('painel/login', [
    'as'   => 'painel.auth',
    'uses' => 'Painel\HomeController@attempt'
]);
Route::get('painel/logout', [
    'as'   => 'painel.logout',
    'uses' => 'Painel\HomeController@logout'
]);

Route::group(['prefix' => 'painel', 'before' => 'auth'], function() {
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    Route::resource('perfil', 'Painel\PerfilController');

    Route::resource('decor', 'Painel\DecorController');
    Route::resource('decor.imagens', 'Painel\DecorImagensController');

    Route::resource('cenografia', 'Painel\CenografiaController');
    Route::resource('cenografia.imagens', 'Painel\CenografiaImagensController');

    Route::resource('produtos', 'Painel\ProdutosController');
    Route::resource('produtos.imagens', 'Painel\ProdutosImagensController');

    Route::resource('imprensa', 'Painel\ImprensaController');
    Route::resource('imprensa.imagens', 'Painel\ImprensaImagensController');

    Route::resource('blog/categorias', 'Painel\BlogCategoriasController');
    Route::resource('blog', 'Painel\BlogController');
    Route::post('blog/{post}/comentarios/{id}/aprovacao/{valor}', [
        'as'   => 'painel.blog.comentarios.aprovacao',
        'uses' => 'Painel\BlogComentariosController@aprovacao'
    ]);
    Route::resource('blog.comentarios', 'Painel\BlogComentariosController');

    Route::resource('contato/recebidos', 'Painel\ContatosRecebidosController');
    Route::resource('contato/creditos', 'Painel\CreditosController');
    Route::resource('contato', 'Painel\ContatoController');

    Route::resource('usuarios', 'Painel\UsuariosController');

    Route::post('ajax/order', 'Painel\AjaxController@order');
});
