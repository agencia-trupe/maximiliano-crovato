@section('content')

    <legend>
        <h2><small>Cenografia /</small> Editar Projeto</h2>
    </legend>

    {{ Form::model($cenografia, [
        'route' => ['painel.cenografia.update', $cenografia->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.cenografia._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
