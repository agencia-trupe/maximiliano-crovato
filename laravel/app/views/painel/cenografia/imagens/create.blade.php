@section('content')

    <legend>
        <h2><small>Cenografia / Adicionar Imagem ao Projeto:</small> {{ $cenografia->titulo }}</h2>
    </legend>

    {{ Form::open(['route' => ['painel.cenografia.imagens.store', $cenografia->id], 'files' => true]) }}

        @include('painel.cenografia.imagens._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
