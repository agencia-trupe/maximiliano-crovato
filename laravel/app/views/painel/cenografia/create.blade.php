@section('content')

    <legend>
        <h2><small>Cenografia /</small> Adicionar Projeto</h2>
    </legend>

    {{ Form::open(['route' => 'painel.cenografia.store', 'files' => true]) }}

        @include('painel.cenografia._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
