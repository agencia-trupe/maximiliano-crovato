@section('content')

    <legend>
        <h2><small>Imprensa / Adicionar Imagem ao Projeto:</small> {{ $imprensa->titulo }}</h2>
    </legend>

    {{ Form::open(['route' => ['painel.imprensa.imagens.store', $imprensa->id], 'files' => true]) }}

        @include('painel.imprensa.imagens._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
