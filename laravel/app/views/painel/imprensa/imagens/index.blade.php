@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="alert alert-block alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <a href="{{ route('painel.imprensa.index') }}" title="Voltar para Imprensa" class="btn btn-default">&larr; Voltar para Imprensa</a>

    <legend>
        <h2>
            <small>Imprensa / Imagens do Projeto:</small> {{ $imprensa->titulo }}
            <a href="{{ URL::route('painel.imprensa.imagens.create', $imprensa->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span>  Adicionar Imagem</a>
        </h2>
    </legend>

    @if(count($imagens))
    <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="imprensa_imagens">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Imagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($imagens as $imagem)

                <tr class="tr-row" id="id_{{ $imagem->id }}">
                    <td>
                        <a href="#" class="btn btn-info btn-sm btn-move">
                            <span class="glyphicon glyphicon-move"></span>
                        </a>
                    </td>
                    <td><img src="{{ url('assets/img/imprensa/'.$imagem->imagem) }}" alt="" style="width:100%;max-width:200px;height:auto;"></td>
                    <td class="crud-actions">
                       {{ Form::open(array('route' => array('painel.imprensa.imagens.destroy', $imprensa->id, $imagem->id), 'method' => 'delete')) }}
                        <input type="hidden" name="imprensa_id" value="{{ $imprensa->id }}">
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                       {{ Form::close() }}
                    </td>
                </tr>

            @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-warning" role="alert">Nenhuma imagem cadastrada.</div>
    @endif

</div>

@stop
