@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('email', 'E-mail') }}
    {{ Form::email('email', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('telefone', 'Telefone') }}
    {{ Form::text('telefone', null, ['class' => 'form-control']) }}
</div>

<hr>

<div class="form-group">
    {{ Form::label('assessoria_titulo', 'Assessoria (título)') }}
    {{ Form::text('assessoria_titulo', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('assessoria_email', 'Assessoria (e-mail)') }}
    {{ Form::email('assessoria_email', null, ['class' => 'form-control']) }}
</div>

<hr>

<div class="form-group">
    {{ Form::label('facebook', 'Facebook') }}
    {{ Form::text('facebook', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('instagram', 'Instagram') }}
    {{ Form::text('instagram', null, ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}
