@section('content')

    <legend>
        <h2><small>Contato /</small> Informações de Contato</h2>
    </legend>

    {{ Form::model($contato, [
        'route' => ['painel.contato.update', $contato->id],
        'method' => 'patch'])
    }}

        @include('painel.contato._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop