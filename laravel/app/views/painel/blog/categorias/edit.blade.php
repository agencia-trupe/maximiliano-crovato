@section('content')

    <legend>
        <h2><small>Blog /</small> Editar Categoria</h2>
    </legend>

    {{ Form::model($categoria, [
        'route' => ['painel.blog.categorias.update', $categoria->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.blog.categorias._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
