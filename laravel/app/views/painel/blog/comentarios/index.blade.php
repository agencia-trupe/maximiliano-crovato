@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="alert alert-block alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <a href="{{ route('painel.blog.index') }}" title="Voltar para Blog" class="btn btn-default">&larr; Voltar para Blog</a>

    <legend>
        <h2>
            <small>Blog / Comentários do Post:</small> {{ $post->titulo }}
        </h2>
    </legend>

    @if(count($comentarios))
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th style="width:100px;text-align:center;">Publicado</th>
                <th>Data</th>
                <th>Autor</th>
                <th>E-mail</th>
                <th>Comentário</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($comentarios as $comentario)

                <tr class="tr-row {{ ($comentario->aprovado ? 'success' : 'warning') }}">
                    <td style="width:100px;text-align:center;">
                        @if($comentario->aprovado)
                        <span class="text-success glyphicon glyphicon-ok"></span>
                        {{ Form::open(array('route' => array('painel.blog.comentarios.aprovacao', $post->id, $comentario->id, 0))) }}
                         <button type="submit" class="btn btn-default btn-sm" style="margin-top:10px;"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Omitir</button>
                        {{ Form::close() }}
                        @else
                        <span class="text-danger glyphicon glyphicon-remove"></span>
                        {{ Form::open(array('route' => array('painel.blog.comentarios.aprovacao', $post->id, $comentario->id, 1))) }}
                         <button type="submit" class="btn btn-default btn-sm" style="margin-top:10px;"><span class="glyphicon glyphicon-ok" style="margin-right:10px;"></span>Aprovar</button>
                        {{ Form::close() }}
                        @endif
                    </td>
                    <td>{{ $comentario->data }}</td>
                    <td>{{{ $comentario->autor }}}</td>
                    <td>{{{ $comentario->email }}}</td>
                    <td>{{{ $comentario->comentario }}}</td>
                    <td class="crud-actions">
                       {{ Form::open(array('route' => array('painel.blog.comentarios.destroy', $post->id, $comentario->id), 'method' => 'delete')) }}
                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                       {{ Form::close() }}
                    </td>
                </tr>

            @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-warning" role="alert">Nenhum comentário enviado.</div>
    @endif

</div>

@stop
