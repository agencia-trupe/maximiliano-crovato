@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('blog_categoria_id', 'Categoria') }}
    {{ Form::select('blog_categoria_id', ['' => 'Selecione'] + $categorias, null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('data', 'Data') }}
    {{ Form::text('data', null, ['class' => 'form-control', 'id' => 'datepicker-blog']) }}
</div>

<div class="form-group">
    {{ Form::label('titulo', 'Título') }}
    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ Form::label('capa', 'Imagem de capa') }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/blog/capa/'.$post->capa) }}" style="display:block; margin-bottom: 10px; max-width:300px;height:auto;">
@endif
    {{ Form::file('capa', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('texto', 'Texto') }}
    {{ Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'blog']) }}
</div>

<div class="well">
    {{ Form::checkbox('publicar', 1, null, ['id' => 'publicar']) }}
    {{ Form::label('publicar', 'Publicar') }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.blog.index') }}" class="btn btn-default btn-voltar">Voltar</a>
