@section('content')

    <legend>
        <h2><small>Blog /</small> Adicionar Post</h2>
    </legend>

    {{ Form::open(['route' => 'painel.blog.store', 'files' => true]) }}

        @include('painel.blog._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
