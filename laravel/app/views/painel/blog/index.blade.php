@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="alert alert-block alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <legend>
        <h2>
            Blog
            <div class="btn-group pull-right">
                <a href="{{ route('painel.blog.categorias.index') }}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span>Editar Categorias</a>
                <a href="{{ route('painel.blog.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Adicionar Post</a>
            </div>
        </h2>
    </legend>

    <div class="form-group" style="width:30%;min-width:200px">
        {{ Form::select('filtro', ['' => 'Todas as Categorias'] + $categorias, Input::get('filtro'), ['class' => 'form-control', 'id' => 'filtro-select']) }}
    </div>

    @if(count($blog))
    <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="blog">
        <thead>
            <tr>
                <th style="width:100px;text-align:center;">Publicado</th>
                <th>Data</th>
                <th>Título</th>
                <th>Categoria</th>
                <th>Comentários</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($blog as $post)

            <tr class="tr-row" id="id_{{ $post->id }}">
                <td style="width:100px;text-align:center;">
                    @if($post->publicar)
                    <span class="text-success glyphicon glyphicon-ok"></span>
                    @else
                    <span class="text-danger glyphicon glyphicon-remove"></span>
                    @endif
                </td>
                <td>{{ $post->data }}</td>
                <td>{{ $post->titulo }}</td>
                <td>@if($post->categoria){{ $post->categoria->titulo }}@endif</td>
                <td><a href="{{ route('painel.blog.comentarios.index', $post->id) }}" class="btn btn-default btn-sm @if(!count($post->comentarios)) disabled @endif">
                    <span class="glyphicon glyphicon-comment" style="margin-right:10px;"></span>Gerenciar
                    @if(count($post->comentarios))
                    <span class="badge" style="margin-left:10px;">
                        {{ count($post->comentarios) }}
                    </span>
                    @endif
                </a></td>
                <td class="crud-actions">
                    {{ Form::open(array('route' => array('painel.blog.destroy', $post->id), 'method' => 'delete')) }}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.blog.edit', $post->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    {{ $blog->appends(['filtro' => Input::get('filtro')])->links(); }}
    @else
        <div class="alert alert-warning" role="alert">Nenhum post cadastrado.</div>
    @endif

@stop
