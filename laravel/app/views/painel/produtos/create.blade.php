@section('content')

    <legend>
        <h2><small>Produtos /</small> Adicionar Projeto</h2>
    </legend>

    {{ Form::open(['route' => 'painel.produtos.store', 'files' => true]) }}

        @include('painel.produtos._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
