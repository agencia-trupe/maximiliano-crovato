@section('content')

    <legend>
        <h2><small>Produtos / Adicionar Imagem ao Projeto:</small> {{ $produtos->titulo }}</h2>
    </legend>

    {{ Form::open(['route' => ['painel.produtos.imagens.store', $produtos->id], 'files' => true]) }}

        @include('painel.produtos.imagens._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
