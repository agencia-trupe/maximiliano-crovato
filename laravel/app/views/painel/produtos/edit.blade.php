@section('content')

    <legend>
        <h2><small>Produtos /</small> Editar Projeto</h2>
    </legend>

    {{ Form::model($produtos, [
        'route' => ['painel.produtos.update', $produtos->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.produtos._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
