@section('content')

    <legend>
        <h2><small>Decor /</small> Editar Projeto</h2>
    </legend>

    {{ Form::model($decor, [
        'route' => ['painel.decor.update', $decor->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.decor._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
