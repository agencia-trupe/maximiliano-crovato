@section('content')

    <legend>
        <h2><small>Decor /</small> Adicionar Projeto</h2>
    </legend>

    {{ Form::open(['route' => 'painel.decor.store', 'files' => true]) }}

        @include('painel.decor._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
