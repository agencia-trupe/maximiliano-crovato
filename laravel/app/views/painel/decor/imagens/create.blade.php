@section('content')

    <legend>
        <h2><small>Decor / Adicionar Imagem ao Projeto:</small> {{ $decor->titulo }}</h2>
    </legend>

    {{ Form::open(['route' => ['painel.decor.imagens.store', $decor->id], 'files' => true]) }}

        @include('painel.decor.imagens._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
