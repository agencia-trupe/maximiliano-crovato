@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('funcao', 'Função') }}
    {{ Form::text('funcao', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('responsavel', 'Responsável') }}
    {{ Form::text('responsavel', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('site', 'Endereço do Site (incluir "http://")') }}
    {{ Form::text('site', null, ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.contato.creditos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
