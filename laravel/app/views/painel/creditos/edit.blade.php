@section('content')

    <legend>
        <h2><small>Contato /</small> Editar Crédito</h2>
    </legend>

    {{ Form::model($credito, [
        'route' => ['painel.contato.creditos.update', $credito->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.creditos._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
