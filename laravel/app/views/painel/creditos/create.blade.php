@section('content')

    <legend>
        <h2><small>Contato /</small> Adicionar Crédito</h2>
    </legend>

    {{ Form::open(['route' => 'painel.contato.creditos.store']) }}

        @include('painel.creditos._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
