<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url() }}">{{ Config::get('projeto.name') }}</a>
        </div>

    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li @if(str_is('painel.perfil*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.perfil.index') }}">Perfil</a>
            </li>

            <li @if(str_is('painel.decor*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.decor.index') }}">Decor</a>
            </li>

            <li @if(str_is('painel.cenografia*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.cenografia.index') }}">Cenografia</a>
            </li>

            <li @if(str_is('painel.produtos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.produtos.index') }}">Produtos</a>
            </li>

            <li @if(str_is('painel.imprensa*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.imprensa.index') }}">Imprensa</a>
            </li>

            <li @if(str_is('painel.blog*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.blog.index') }}">Blog</a>
            </li>

            <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contato @if($recebidos_count>=1)<span class="badge">{{ $recebidos_count }}</span>@endif <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
                    <li><a href="{{ route('painel.contato.recebidos.index') }}">Contatos Recebidos @if($recebidos_count>=1)<span class="badge">{{ $recebidos_count }}</span>@endif</a></li>
                    <li><a href="{{ route('painel.contato.creditos.index') }}">Créditos</a></li>
                </ul>
            </li>

            <li class="dropdown @if(str_is('painel.usuarios*', Route::currentRouteName())) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{{ route('painel.usuarios.index') }}">Usuários</a></li>
                    <li><a href="{{ route('painel.logout') }}">Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
    </div>
</nav>
