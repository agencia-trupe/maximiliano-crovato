<div class="main-nav">
    <div class="inner">
        <h1><a href="{{ route('home') }}">{{ Config::get('projeto.title') }}</a></h1>
        <nav>
            <a href="{{ route('perfil') }}" @if(str_is('perfil*', Route::currentRouteName())) class='active' @endif>Max</a>
            <a href="{{ route('decor') }}" @if(str_is('decor*', Route::currentRouteName())) class='active' @endif>Décor</a>
            <a href="{{ route('cenografia') }}" @if(str_is('cenografia*', Route::currentRouteName())) class='active' @endif>Cenografia</a>
            <a href="{{ route('produtos') }}" @if(str_is('produtos*', Route::currentRouteName())) class='active' @endif>Produtos</a>
            <a href="{{ route('imprensa') }}" @if(str_is('imprensa*', Route::currentRouteName())) class='active' @endif>Clipping</a>
            <a href="{{ route('blog') }}">Max Blog</a>
            <a href="{{ route('contato') }}" @if(str_is('contato*', Route::currentRouteName())) class='active' @endif>Contato</a>
        </nav>
    </div>
</div>
<div class="content">
    <div class="content-wrapper">
        <div class="inner {{ Route::currentRouteName() }}">
            @yield('content')
        </div>
    </div>
</div>
