<div class="navbar">
    <a href="{{ route($categoria) }}" class="voltar">voltar</a>
    <a href="#" class="carousel-controler" id="carousel-prev">prev</a>
    <a href="#" class="carousel-controler" id="carousel-next">next</a>
</div>

<div class="carousel-wrap">
    <div class="carousel">
        @foreach($projetos as $projeto)
        <a href="{{ route($categoria, $projeto->slug) }}" @if($projeto->id == $selecionado->id) class="active" id="selecionado" @endif>
            <img src="{{ asset('assets/img/'.$categoria.'/capa/'.$projeto->capa) }}" alt="">
            <div class="overlay">
                <span>
                    {{ $projeto->titulo }}
                </span>
            </div>
        </a>
        @endforeach
    </div>
</div>

<div class="galeria-wrap">
    @yield('content')
</div>
