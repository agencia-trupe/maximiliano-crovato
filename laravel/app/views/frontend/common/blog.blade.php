<div class="center">
    <div class="blog-sidebar">
        <h1><a href="{{ route('home') }}">{{ Config::get('projeto.title') }}</a></h1>
        <h2><a href="{{ route('blog') }}">Max Blog</a></h2>

        <ul class="categorias">
            @foreach($categorias as $categoria)
            <li>
                <a href="{{ route('blog.cat', $categoria->slug) }}" @if($categoria->id == $categoria_id) class="active" @endif>
                    {{ $categoria->titulo }}
                </a>
            </li>
            @endforeach
        </ul>

        <div class="datas">
            <?php $ano = null ?>
            @foreach($datas as $data)
                @if($ano != $data->ano)
                    <a href="{{ route('blog.data', $data->ano) }}" class="ano @if($data->ano == $ano_selecionado) active @endif">
                        {{ $data->ano }}
                    </a>
                    <?php $ano = $data->ano ?>
                @endif
                <a href="{{ route('blog.data', [$data->ano, $data->mes]) }}" class="mes @if($data-> ano == $ano_selecionado && $data->mes == $mes_selecionado) active @endif">
                    [{{ Tools::mesExtenso($data->mes) }}]
                </a>
            @endforeach
        </div>
    </div>

    <div class="blog-main">
        @yield('content')
    </div>
</div>
