@section('content')

    <a href="#" class="perfil-seta"></a>

    <div class="imagem" style="background-image: url('{{ asset('assets/img/perfil/'.$perfil->imagem) }}')"></div>

    <div class="texto">
        {{ $perfil->texto }}
    </div>

@stop
