@section('content')

    @foreach($imprensa as $projeto)
    <a href="#" class="thumb" data-galeria="{{ $projeto->id }}">
        <img src="{{ asset('assets/img/imprensa/capa/'.$projeto->capa) }}" alt="">
        <div class="overlay">
            <span>
                {{ $projeto->titulo }}<br>
                {{ Tools::formataDataImprensa($projeto->data) }}
            </span>
        </div>
    </a>
    @endforeach

    <div class="hidden">
    @foreach($imprensa as $projeto)
        @foreach($projeto->imagens as $imagem)
            <a href="{{ asset('assets/img/imprensa/'.$imagem->imagem) }}" class="fancybox" rel="galeria{{ $projeto->id }}"></a>
        @endforeach
    @endforeach
    </div>

@stop
