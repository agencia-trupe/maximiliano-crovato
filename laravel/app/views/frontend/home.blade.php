@section('content')

    <ul class="social">
        @if($contato->facebook)
        <li><a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a></li>
        @endif
        @if($contato->instagram)
        <li><a href="{{ $contato->instagram }}" class="instagram" target="_blank">instagram</a></li>
        @endif
    </ul>

@stop
