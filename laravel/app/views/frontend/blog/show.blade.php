@section('content')

    <div class="blog-post">
        <h3>{{ $post->titulo }}</h3>

        <div class="info">
            <span class="left">
                {{ Tools::formataDataBlog($post->data) }} · <a href="{{ route('blog.cat', $post->categoria->slug) }}">{{ $post->categoria->titulo }}</a>
            </span>
            <span class="right">
                {{ count($post->comentariosAprovados) }} Comentário{{ (count($post->comentariosAprovados) != 1 ? 's' : '') }} · <a href="#" id="comente">Comente!</a>
            </span>
        </div>

        <div class="texto">
            {{ $post->texto }}
        </div>

        <div id="comentarios">
            <h5>COMENTÁRIOS [{{ count($post->comentariosAprovados) }}]</h5>

            <form action="" id="form-comentario">
                <input type="text" name="autor" id="autor" placeholder="NOME" required>
                <input type="email" name="email" id="email" placeholder="E-MAIL" required>
                <textarea name="comentario" id="comentario" placeholder="COMENTÁRIO" required></textarea>
                <input type="hidden" name="blog_id" id="blog_id" value="{{ $post->id }}">
                <input type="submit" value="[ ENVIAR ]">
                <p>Os comentários são moderados pela nossa equipe que pode optar por não exibí-los se assim achar conveniente, não cabendo ao autor do comentário qualquer alegação.</p>
                <div id="form-resposta"></div>
            </form>

            <ul class="comentarios-lista">
                @foreach($post->comentariosAprovados as $comentario)
                    <li>
                        <span class="autor">
                            {{ Tools::formataDataBlog($comentario->data) }} · {{{ $comentario->autor }}}
                        </span>
                        <span class="comentario">
                            {{{ $comentario->comentario }}}
                        </span>
                    </li>
                @endforeach
            </ul>
        </div>

        <a href="#" id="topo">Topo</a>
        <div class="paginacao">
            @if($anterior)
            <a href="{{ route('blog', $anterior->slug) }}" class="prev">Post Anterior</a>
            @endif
            @if($proximo)
            <a href="{{ route('blog', $proximo->slug) }}" class="next">Próximo Post</a>
            @endif
        </div>
    </div>

@stop
