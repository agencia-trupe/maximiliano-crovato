@section('content')

@if(count($posts))
    <div class="blog-thumbs">
        @foreach($posts as $post)
        <a href="{{ route('blog', $post->slug) }}" class="thumb">
            <img src="{{ asset('assets/img/blog/capa/'.$post->capa) }}" alt="">
            <div class="overlay">
                <span class="titulo">{{ $post->titulo }}</span>
                <span class="info">
                     {{ Tools::formataDataBlog($post->data) }} · {{ $post->categoria->titulo }}
                </span>
            </div>
        </a>
        @endforeach

    </div>
    {{ $posts->links('pagination::simple') }}
@else
    <h3 class="not-found">Nenhum post encontrado.</h3>
@endif

@stop
