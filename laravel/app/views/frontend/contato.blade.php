@section('content')

    <h2>Fale conosco</h2>

    <form action="" id="form-contato">
        <input type="text" name="nome" id="nome" placeholder="NOME" required>
        <input type="email" name="email" id="email" placeholder="E-MAIL" required>
        <input type="text" name="telefone" id="telefone" placeholder="TELEFONE">
        <textarea name="mensagem" id="mensagem" placeholder="MENSAGEM" required></textarea>
        <input type="submit" value="[ ENVIAR ]">
        <div id="form-resposta-wrapper">
            <div id="form-resposta"></div>
        </div>
    </form>

    <div class="info">
        <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
        <span class="telefone">{{ $contato->telefone }}</span>
        <ul class="social">
            @if($contato->facebook)
            <li><a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a></li>
            @endif
            @if($contato->instagram)
            <li><a href="{{ $contato->instagram }}" class="instagram" target="_blank">instagram</a></li>
            @endif
        </ul>
        <a href="mailto:{{ $contato->assessoria_email }}" class="assessoria">
            <span>assessoria de imprensa</span>
            {{ $contato->assessoria_titulo }}
        </a>
    </div>

    <div class="creditos">
        <span>Créditos</span>
        @foreach($creditos as $credito)
        <p>{{ $credito->funcao }} · {{ $credito->responsavel }} · <a href="{{ $credito->site }}" target="_blank">{{ str_replace(['http://', 'https://'], '', $credito->site) }}</a></p>
        @endforeach
    </div>

@stop
