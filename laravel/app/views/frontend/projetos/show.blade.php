@section('content')

<div class="galeria">
    <div class="galeria-slider">
    @foreach($selecionado->imagens as $imagem)
        <div class="slide {{ Tools::aspectRatio(asset('assets/img/'.$categoria.'/'.rawurlencode($imagem->imagem))) }}" style="background-image: url('{{ asset('assets/img/'.$categoria.'/'.$imagem->imagem) }}')"></div>
    @endforeach
    </div>

    <a href="#" class="galeria-controler" id="galeria-prev">prev</a>
    <a href="#" class="galeria-controler" id="galeria-next">next</a>
</div>

@stop
