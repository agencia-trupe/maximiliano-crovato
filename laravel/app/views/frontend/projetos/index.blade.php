@section('content')

    @foreach($projetos as $projeto)
    <a href="{{ route($categoria, $projeto->slug) }}" class="thumb">
        <img src="{{ asset('assets/img/'.$categoria.'/capa/'.$projeto->capa) }}" alt="">
        <div class="overlay">
            <span>
                {{ $projeto->titulo }}
            </span>
        </div>
    </a>
    @endforeach

@stop
