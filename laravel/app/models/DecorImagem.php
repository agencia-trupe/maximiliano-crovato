<?php

class DecorImagem extends Eloquent
{

    protected $table = 'decor_imagens';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeProjeto($query, $id)
    {
        return $query->where('decor_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

}
