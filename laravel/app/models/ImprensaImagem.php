<?php

class ImprensaImagem extends Eloquent
{

    protected $table = 'imprensa_imagens';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeProjeto($query, $id)
    {
        return $query->where('imprensa_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

}
