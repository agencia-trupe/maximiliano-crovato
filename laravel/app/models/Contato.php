<?php

class Contato extends Eloquent
{

    protected $table = 'contato';

    protected $hidden = [];

    protected $guarded = ['id'];

}
