<?php

class CenografiaImagem extends Eloquent
{

    protected $table = 'cenografia_imagens';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeProjeto($query, $id)
    {
        return $query->where('cenografia_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

}
