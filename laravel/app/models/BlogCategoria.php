<?php

class BlogCategoria extends Eloquent
{

    protected $table = 'blog_categorias';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function posts()
    {
        return $this->hasMany('Blog', 'blog_categoria_id');
    }

}
