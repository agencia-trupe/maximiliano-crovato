<?php

class ContatoRecebido extends Eloquent
{

    protected $table = 'contatos_recebidos';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

    public function scopeNaoLidos($query)
    {
        return $query->where('lido', '!=', 1);
    }

}
