<?php

class Blog extends Eloquent
{

    protected $table = 'blog';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('blog_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('BlogCategoria', 'blog_categoria_id');
    }

    public function scopePublicados($query)
    {
        return $query->where('publicar', 1);
    }

    public function comentarios()
    {
        return $this->hasMany('BlogComentario', 'blog_id')->orderBy('data', 'DESC');
    }

    public function comentariosAprovados()
    {
        return $this->hasMany('BlogComentario', 'blog_id')->orderBy('data', 'DESC')->where('aprovado', 1);
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon\Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

}
