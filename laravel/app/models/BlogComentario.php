<?php

class BlogComentario extends Eloquent
{

    protected $table = 'blog_comentarios';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopePost($query, $id)
    {
        return $query->where('blog_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC');
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon\Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

}
