<?php

class Cenografia extends Eloquent
{

    protected $table = 'cenografia';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function imagens()
    {
        return $this->hasMany('CenografiaImagem', 'cenografia_id')->orderBy('ordem', 'ASC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

}
