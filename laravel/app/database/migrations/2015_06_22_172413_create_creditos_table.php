<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('creditos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ordem')->default(0);
			$table->string('funcao');
			$table->string('responsavel');
			$table->string('site');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('creditos');
	}

}
