<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogComentariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blog_comentarios', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('blog_id');
			$table->string('data');
			$table->string('autor');
			$table->string('email');
			$table->text('comentario');
			$table->boolean('aprovado')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blog_comentarios');
	}

}
