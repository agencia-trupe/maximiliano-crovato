<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        DB::table('contato')->delete();

        $data = array(
            array(
                'email' => 'info@maximilianocrovato.com.br',
                'telefone' => '+55 11 2478 5232',
                'assessoria_titulo' => 'Mariana Amaral Comunicaçao',
                'assessoria_email' => 'email@assessoria.com.br',
                'facebook' => 'facebook',
                'instagram' => 'instagram',
            )
        );

        DB::table('contato')->insert($data);
    }

}
