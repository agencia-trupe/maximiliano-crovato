<?php

class PerfilSeeder extends Seeder {

    public function run()
    {
        DB::table('perfil')->delete();

        $data = array(
            array(
                'imagem' => 'placeholder.jpg',
                'texto'  => 'Texto Perfil',
            )
        );

        DB::table('perfil')->insert($data);
    }

}
