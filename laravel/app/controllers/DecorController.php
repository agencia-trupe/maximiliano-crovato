<?php

class DecorController extends BaseController {

    public function index($slug = null)
    {
        View::share('categoria', 'decor');
        View::share('projetos', Decor::ordenados()->get());

        if (!$slug) {

            return $this->view('frontend.projetos.index');

        } else {

            $selecionado = Decor::with('imagens')->slug($slug)->first();
            if (!$selecionado) App::abort('404');

            View::share('custom_layout', 'projeto');
            View::share('selecionado', $selecionado);
            return $this->view('frontend.projetos.show');

        }
    }

}
