<?php

class CenografiaController extends BaseController {

    public function index($slug = null)
    {
        View::share('categoria', 'cenografia');
        View::share('projetos', Cenografia::ordenados()->get());

        if (!$slug) {

            return $this->view('frontend.projetos.index');

        } else {

            $selecionado = Cenografia::with('imagens')->slug($slug)->first();
            if (!$selecionado) App::abort('404');

            View::share('custom_layout', 'projeto');
            View::share('selecionado', $selecionado);
            return $this->view('frontend.projetos.show');

        }
    }

}
