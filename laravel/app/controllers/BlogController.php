<?php

class BlogController extends BaseController {

    public function __construct()
    {
        View::share('custom_layout', 'blog');

        $categorias = BlogCategoria::whereHas('posts', function($query) {
            $query->publicados();
        })->get();
        View::share('categorias', $categorias);
        View::share('categoria_id', null);

        $datas = Blog::publicados()
                       ->select(\DB::raw('YEAR(data) as ano, MONTH(data) as mes'))
                       ->groupBy('ano', 'mes')
                       ->orderBy('ano', 'DESC')
                       ->orderBy('mes', 'DESC')
                       ->get();
        View::share('datas', $datas);
        View::share('ano_selecionado', null);
        View::share('mes_selecionado', null);
    }

    public function index($slug = null)
    {
        if (!$slug) {

            $posts = Blog::with('comentariosAprovados')->ordenados()->publicados()->paginate(15);

            return $this->view('frontend.blog.index', compact('posts'));

        } else {

            $post = Blog::with('comentariosAprovados')->slug($slug)->first();
            if (!$post) return $this->view('frontend.blog.index', ['posts' => null]);

            $data_post = Carbon\Carbon::createFromFormat('d/m/Y', $post->data)->format('Y-m-d');
            $anterior = Blog::select('slug')
                              ->publicados()
                              ->where('data', '<', $data_post)
                              ->orWhere(function($query) use ($data_post, $post) {
                                  $query->where('data', $data_post)
                                        ->where('id', '<', $post->id);
                              })
                              ->orderBy('data', 'DESC')
                              ->orderBy('id', 'DESC')
                              ->first();
            $proximo  = Blog::select('slug')
                              ->publicados()
                              ->where('data', '>', $data_post)
                              ->orWhere(function($query) use ($data_post, $post) {
                                  $query->where('data', $data_post)
                                        ->where('id', '>', $post->id);
                              })
                              ->orderBy('data', 'ASC')
                              ->orderBy('id', 'ASC')
                              ->first();

            return $this->view('frontend.blog.show', compact('post', 'anterior', 'proximo'));
        }
    }

    public function filtroCategoria($slug = null)
    {
        $categoria = BlogCategoria::slug($slug)->first();
        if (!$categoria) return $this->view('frontend.blog.index', ['posts' => null]);

        $posts = Blog::with('comentariosAprovados')->categoria($categoria->id)->ordenados()->publicados()->paginate(15);

        View::share('categoria_id', $categoria->id);
        return $this->view('frontend.blog.index', compact('posts'));
    }

    public function filtroData($ano = null, $mes = null)
    {
        if (!$ano) return $this->view('frontend.blog.index', ['posts' => null]);

        if (!$mes) {
            $posts = Blog::with('comentariosAprovados')->ordenados()->publicados()
                           ->whereRaw("YEAR(data) = $ano")
                           ->paginate(15);
        } else {
            $posts = Blog::with('comentariosAprovados')->ordenados()->publicados()
                           ->whereRaw("YEAR(data) = $ano AND MONTH(data) = $mes")
                           ->paginate(15);
        }

        View::share('ano_selecionado', $ano);
        View::share('mes_selecionado', $mes);
        return $this->view('frontend.blog.index', compact('posts'));
    }

    public function comentario()
    {
        $autor = Input::get('autor');
        $email = Input::get('email');
        $comentario = Input::get('comentario');
        $blog_id = Input::get('blog_id');

        $validation = Validator::make(
            array(
                'autor'      => $autor,
                'email'      => $email,
                'comentario' => $comentario
            ),
            array(
                'autor'      => 'required',
                'email'      => 'required|email',
                'comentario' => 'required'
            )
        );

        if ($validation->fails())
        {
            Debugbar::info($validation->messages());
            $response = array(
                'status'  => 'fail',
                'message' => 'Preencha todos os campos corretamente.'
            );
            return Response::json($response);
        }

        try {

            $object = new BlogComentario;
            $object->blog_id = $blog_id;
            $object->data = Carbon\Carbon::now()->format('d/m/Y');
            $object->autor = $autor;
            $object->email = $email;
            $object->comentario = $comentario;
            $object->save();

            $response = array(
                'status'  => 'success',
                'message' => 'Comentário enviado com sucesso! Aguardando aprovação.'
            );
            return Response::json($response);

        } catch (\Exception $e) {
            Debugbar::info($e);
            $response = array(
                'status'  => 'fail',
                'message' => 'Ocorreu um erro. Tente novamente mais tarde.'
            );
            return Response::json($response);

        }
    }

}
