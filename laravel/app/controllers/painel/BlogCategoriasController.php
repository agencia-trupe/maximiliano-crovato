<?php

namespace Painel;

use \BlogCategoria, \Input, \Validator, \Redirect, \Session, \Str;

class BlogCategoriasController extends BasePainelController {

    private $validation_rules = [
        'titulo' => 'required',
    ];

    public function index()
    {
        $categorias = BlogCategoria::ordenados()->get();

        return $this->view('painel.blog.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return $this->view('painel.blog.categorias.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));
            BlogCategoria::create($input);
            Session::flash('sucesso', 'Categoria adicionada com sucesso.');

            return Redirect::route('painel.blog.categorias.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao adicionar categoria. Verifique se já existe outra categoria com o mesmo título.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $categoria = BlogCategoria::findOrFail($id);

        return $this->view('painel.blog.categorias.edit', compact('categoria'));
    }

    public function update($id)
    {
        $categoria = BlogCategoria::findOrFail($id);
        $input     = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));
            $categoria->update($input);
            Session::flash('sucesso', 'Categoria alterada com sucesso.');

            return Redirect::route('painel.blog.categorias.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar categoria. Verifique se já existe outra categoria com o mesmo título.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            BlogCategoria::destroy($id);
            Session::flash('sucesso', 'Categoria removida com sucesso.');

            return Redirect::route('painel.blog.categorias.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover categoria.']);

        }
    }

}
