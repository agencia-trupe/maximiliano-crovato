<?php

namespace Painel;

use \Blog, \BlogCategoria, \Input, \Validator, \Redirect, \Session, \Str, \CropImage;

class BlogController extends BasePainelController {

    private $validation_rules = [
        'blog_categoria_id' => 'required',
        'data'   => 'required',
        'titulo' => 'required',
        'texto'  => 'required',
        'capa'   => 'required|image',
    ];

    private $image_config = [
        'width'  => 400,
        'height' => null,
        'path'   => 'assets/img/blog/capa/'
    ];

    public function index()
    {
        $categorias = BlogCategoria::lists('titulo', 'id');

        if (Input::has('filtro') && BlogCategoria::find(Input::get('filtro'))) {
            $filtro = Input::get('filtro');
            $blog   = Blog::with('comentarios')->ordenados()->categoria($filtro)->paginate(20);
        } else {
            $filtro = null;
            $blog   = Blog::with('comentarios')->ordenados()->paginate(20);
        }

        return $this->view('painel.blog.index', compact('blog', 'categorias', 'filtro'));
    }

    public function create()
    {
        $categorias = BlogCategoria::lists('titulo', 'id');

        return $this->view('painel.blog.create', compact('categorias'));
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));
            $input['capa'] = CropImage::make('capa', $this->image_config);

            Blog::create($input);
            Session::flash('sucesso', 'Post adicionado com sucesso.');

            return Redirect::route('painel.blog.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar post. Verifique se já existe outro post com o mesmo título.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $post = Blog::findOrFail($id);
        $categorias = BlogCategoria::lists('titulo', 'id');

        return $this->view('painel.blog.edit', compact('post', 'categorias'));
    }

    public function update($id)
    {
        $post  = Blog::findOrFail($id);
        $input = Input::all();

        $this->validation_rules['capa'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));

            if (Input::hasFile('capa')) {
                $input['capa'] = CropImage::make('capa', $this->image_config);
            } else {
                unset($input['capa']);
            }

            $input['publicar'] = (Input::has('publicar') ? 1 : 0);

            $post->update($input);
            Session::flash('sucesso', 'Post alterado com sucesso.');

            return Redirect::route('painel.blog.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar post. Verifique se já existe outro post com o mesmo título.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Blog::destroy($id);
            Session::flash('sucesso', 'Post removido com sucesso.');

            return Redirect::route('painel.blog.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover post.']);

        }
    }

}
