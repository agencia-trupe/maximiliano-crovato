<?php

namespace Painel;

use \Decor, \Input, \Session, \Redirect, \Validator, \Str, \CropImage;

class DecorController extends BasePainelController {

    private $validation_rules = [
        'titulo' => 'required',
        'capa'   => 'required|image'
    ];

    private $image_config = [
        'width'  => 300,
        'height' => 300,
        'path'   => 'assets/img/decor/capa/'
    ];

    public function index()
    {
        $decor = Decor::ordenados()->get();

        return $this->view('painel.decor.index', compact('decor'));
    }

    public function create()
    {
        return $this->view('painel.decor.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));
            $input['capa'] = CropImage::make('capa', $this->image_config);
            Decor::create($input);
            Session::flash('sucesso', 'Projeto criado com sucesso.');

            return Redirect::route('painel.decor.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar projeto. Verifique se já existe outro projeto com o mesmo título.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $decor = Decor::findOrFail($id);

        return $this->view('painel.decor.edit', compact('decor'));
    }

    public function update($id)
    {
        $decor = Decor::findOrFail($id);
        $input   = Input::all();

        $this->validation_rules['capa'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));

            if (Input::hasFile('capa')) {
                $input['capa'] = CropImage::make('capa', $this->image_config);
            } else {
                unset($input['capa']);
            }

            $decor->update($input);
            Session::flash('sucesso', 'Projeto alterado com sucesso.');

            return Redirect::route('painel.decor.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar projeto. Verifique se já existe outro projeto com o mesmo título.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Decor::destroy($id);
            Session::flash('sucesso', 'Projeto removido com sucesso.');

            return Redirect::route('painel.decor.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover projeto.']);

        }
    }

}
