<?php

namespace Painel;

use \Perfil, \Input, \Session, \Redirect, \Validator, \CropImage;

class PerfilController extends BasePainelController {

    private $validation_rules = [
        'imagem' => 'image',
        'texto'  => 'required',
    ];

    private $image_config = [
        'width'  => 2000,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/perfil/'
    ];

    public function index()
    {
        $perfil = Perfil::first();

        return $this->view('painel.perfil.index', compact('perfil'));
    }

    public function update($id)
    {
        $perfil = Perfil::findOrFail($id);
        $input  = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        if (Input::hasFile('imagem')) {
            $input['imagem'] = CropImage::make('imagem', $this->image_config);
        } else {
            unset($input['imagem']);
        }

        try {

            $perfil->update($input);
            Session::flash('sucesso', 'Página alterada com sucesso.');

            return Redirect::route('painel.perfil.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar página.'])
                ->withInput();

        }
    }

}
