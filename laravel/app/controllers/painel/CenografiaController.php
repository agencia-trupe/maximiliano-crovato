<?php

namespace Painel;

use \Cenografia, \Input, \Session, \Redirect, \Validator, \Str, \CropImage;

class CenografiaController extends BasePainelController {

    private $validation_rules = [
        'titulo' => 'required',
        'capa'   => 'required|image'
    ];

    private $image_config = [
        'width'  => 300,
        'height' => 300,
        'path'   => 'assets/img/cenografia/capa/'
    ];

    public function index()
    {
        $cenografia = Cenografia::ordenados()->get();

        return $this->view('painel.cenografia.index', compact('cenografia'));
    }

    public function create()
    {
        return $this->view('painel.cenografia.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));
            $input['capa'] = CropImage::make('capa', $this->image_config);
            Cenografia::create($input);
            Session::flash('sucesso', 'Projeto criado com sucesso.');

            return Redirect::route('painel.cenografia.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar projeto. Verifique se já existe outro projeto com o mesmo título.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $cenografia = Cenografia::findOrFail($id);

        return $this->view('painel.cenografia.edit', compact('cenografia'));
    }

    public function update($id)
    {
        $cenografia = Cenografia::findOrFail($id);
        $input   = Input::all();

        $this->validation_rules['capa'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));

            if (Input::hasFile('capa')) {
                $input['capa'] = CropImage::make('capa', $this->image_config);
            } else {
                unset($input['capa']);
            }

            $cenografia->update($input);
            Session::flash('sucesso', 'Projeto alterado com sucesso.');

            return Redirect::route('painel.cenografia.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar projeto. Verifique se já existe outro projeto com o mesmo título.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Cenografia::destroy($id);
            Session::flash('sucesso', 'Projeto removido com sucesso.');

            return Redirect::route('painel.cenografia.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover projeto.']);

        }
    }

}
