<?php

namespace Painel;

use \Blog, \BlogComentario, \Input, \Session, \Redirect, \Validator, \CropImage;

class BlogComentariosController extends BasePainelController {

    public function index($post_id)
    {
        $post = Blog::find($post_id);
        if(!$post) return Redirect::route('painel.blog.index');

        $comentarios = BlogComentario::post($post_id)->ordenados()->get();

        return $this->view('painel.blog.comentarios.index', compact('post', 'comentarios'));
    }

    public function destroy($blog_id, $comentario_id)
    {
        try {

            BlogComentario::destroy($comentario_id);
            Session::flash('sucesso', 'Comentário removido com sucesso.');

            return Redirect::route('painel.blog.comentarios.index', $blog_id);

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover comentário.']);

        }
    }

    public function aprovacao($post_id, $comentario_id, $valor)
    {
        try {

            $comentario = BlogComentario::find($comentario_id);
            $comentario->aprovado = $valor;
            $comentario->save();
            Session::flash('sucesso', 'Comentário alterado com sucesso.');

            return Redirect::back();

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao alterar comentário.']);

        }
    }

}
