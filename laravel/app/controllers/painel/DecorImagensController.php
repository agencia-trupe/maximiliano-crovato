<?php

namespace Painel;

use \Decor, \DecorImagem, \Input, \Session, \Redirect, \Validator, \CropImage;

class DecorImagensController extends BasePainelController {

    private $validation_rules = [
        'imagem'   => 'required|image'
    ];

    private $image_config = [
        'width'  => 3000,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/decor/'
    ];

    public function index($decor_id)
    {
        $decor = Decor::find($decor_id);
        if(!$decor) return Redirect::route('painel.decor.index');

        $imagens = DecorImagem::projeto($decor_id)->ordenados()->get();

        return $this->view('painel.decor.imagens.index', compact('decor', 'imagens'));
    }

    public function create($decor_id)
    {
        $decor = Decor::find($decor_id);
        if(!$decor) return Redirect::route('painel.decor.index');

        return $this->view('painel.decor.imagens.create', compact('decor'));
    }

    public function store($decor_id)
    {
        $input = Input::all();
        $input['decor_id'] = $decor_id;

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            DecorImagem::create($input);
            Session::flash('sucesso', 'Imagem inserida com sucesso.');

            return Redirect::route('painel.decor.imagens.index', $input['decor_id']);

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao inserir imagem.'])
                ->withInput();

        }
    }

    public function destroy($decor_id, $imagem_id)
    {
        try {

            DecorImagem::destroy($imagem_id);
            Session::flash('sucesso', 'Imagem removida com sucesso.');

            return Redirect::route('painel.decor.imagens.index', $decor_id);

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover imagem.']);

        }
    }

}
