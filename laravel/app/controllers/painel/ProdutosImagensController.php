<?php

namespace Painel;

use \Produto, \ProdutoImagem, \Input, \Session, \Redirect, \Validator, \CropImage;

class ProdutosImagensController extends BasePainelController {

    private $validation_rules = [
        'imagem'   => 'required|image'
    ];

    private $image_config = [
        'width'  => 3000,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/produtos/'
    ];

    public function index($produtos_id)
    {
        $produtos = Produto::find($produtos_id);
        if(!$produtos) return Redirect::route('painel.produtos.index');

        $imagens = ProdutoImagem::projeto($produtos_id)->ordenados()->get();

        return $this->view('painel.produtos.imagens.index', compact('produtos', 'imagens'));
    }

    public function create($produtos_id)
    {
        $produtos = Produto::find($produtos_id);
        if(!$produtos) return Redirect::route('painel.produtos.index');

        return $this->view('painel.produtos.imagens.create', compact('produtos'));
    }

    public function store($produtos_id)
    {
        $input = Input::all();
        $input['produtos_id'] = $produtos_id;

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            ProdutoImagem::create($input);
            Session::flash('sucesso', 'Imagem inserida com sucesso.');

            return Redirect::route('painel.produtos.imagens.index', $input['produtos_id']);

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao inserir imagem.'])
                ->withInput();

        }
    }

    public function destroy($produtos_id, $imagem_id)
    {
        try {

            ProdutoImagem::destroy($imagem_id);
            Session::flash('sucesso', 'Imagem removida com sucesso.');

            return Redirect::route('painel.produtos.imagens.index', $produtos_id);

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover imagem.']);

        }
    }

}
