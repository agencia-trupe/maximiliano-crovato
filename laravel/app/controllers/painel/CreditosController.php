<?php

namespace Painel;

use \Credito, \Input, \Validator, \Redirect, \Session;

class CreditosController extends BasePainelController {

    private $validation_rules = [
        'funcao'      => 'required',
        'responsavel' => 'required',
        'site'        => 'required'
    ];

    public function index()
    {
        $creditos = Credito::ordenados()->get();

        return $this->view('painel.creditos.index', compact('creditos'));
    }

    public function create()
    {
        return $this->view('painel.creditos.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            Credito::create($input);
            Session::flash('sucesso', 'Crédito adicionado com sucesso.');

            return Redirect::route('painel.contato.creditos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao adicionar crédito.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $credito = Credito::findOrFail($id);

        return $this->view('painel.creditos.edit', compact('credito'));
    }

    public function update($id)
    {
        $credito = Credito::findOrFail($id);
        $input   = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $credito->update($input);
            Session::flash('sucesso', 'Crédito alterado com sucesso.');

            return Redirect::route('painel.contato.creditos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar crédito.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Credito::destroy($id);
            Session::flash('sucesso', 'Crédito removido com sucesso.');

            return Redirect::route('painel.contato.creditos.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover crédito.']);

        }
    }

}
