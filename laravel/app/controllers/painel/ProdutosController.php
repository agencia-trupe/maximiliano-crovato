<?php

namespace Painel;

use \Produto, \Input, \Session, \Redirect, \Validator, \Str, \CropImage;

class ProdutosController extends BasePainelController {

    private $validation_rules = [
        'titulo' => 'required',
        'capa'   => 'required|image'
    ];

    private $image_config = [
        'width'  => 300,
        'height' => 300,
        'path'   => 'assets/img/produtos/capa/'
    ];

    public function index()
    {
        $produtos = Produto::ordenados()->get();

        return $this->view('painel.produtos.index', compact('produtos'));
    }

    public function create()
    {
        return $this->view('painel.produtos.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));
            $input['capa'] = CropImage::make('capa', $this->image_config);
            Produto::create($input);
            Session::flash('sucesso', 'Projeto criado com sucesso.');

            return Redirect::route('painel.produtos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar projeto. Verifique se já existe outro projeto com o mesmo título.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $produtos = Produto::findOrFail($id);

        return $this->view('painel.produtos.edit', compact('produtos'));
    }

    public function update($id)
    {
        $produtos = Produto::findOrFail($id);
        $input   = Input::all();

        $this->validation_rules['capa'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));

            if (Input::hasFile('capa')) {
                $input['capa'] = CropImage::make('capa', $this->image_config);
            } else {
                unset($input['capa']);
            }

            $produtos->update($input);
            Session::flash('sucesso', 'Projeto alterado com sucesso.');

            return Redirect::route('painel.produtos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar projeto. Verifique se já existe outro projeto com o mesmo título.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Produto::destroy($id);
            Session::flash('sucesso', 'Projeto removido com sucesso.');

            return Redirect::route('painel.produtos.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover projeto.']);

        }
    }

}
