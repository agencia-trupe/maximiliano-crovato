<?php

namespace Painel;

use \Cenografia, \CenografiaImagem, \Input, \Session, \Redirect, \Validator, \CropImage;

class CenografiaImagensController extends BasePainelController {

    private $validation_rules = [
        'imagem'   => 'required|image'
    ];

    private $image_config = [
        'width'  => 3000,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/cenografia/'
    ];

    public function index($cenografia_id)
    {
        $cenografia = Cenografia::find($cenografia_id);
        if(!$cenografia) return Redirect::route('painel.cenografia.index');

        $imagens = CenografiaImagem::projeto($cenografia_id)->ordenados()->get();

        return $this->view('painel.cenografia.imagens.index', compact('cenografia', 'imagens'));
    }

    public function create($cenografia_id)
    {
        $cenografia = Cenografia::find($cenografia_id);
        if(!$cenografia) return Redirect::route('painel.cenografia.index');

        return $this->view('painel.cenografia.imagens.create', compact('cenografia'));
    }

    public function store($cenografia_id)
    {
        $input = Input::all();
        $input['cenografia_id'] = $cenografia_id;

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            CenografiaImagem::create($input);
            Session::flash('sucesso', 'Imagem inserida com sucesso.');

            return Redirect::route('painel.cenografia.imagens.index', $input['cenografia_id']);

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao inserir imagem.'])
                ->withInput();

        }
    }

    public function destroy($cenografia_id, $imagem_id)
    {
        try {

            CenografiaImagem::destroy($imagem_id);
            Session::flash('sucesso', 'Imagem removida com sucesso.');

            return Redirect::route('painel.cenografia.imagens.index', $cenografia_id);

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover imagem.']);

        }
    }

}
