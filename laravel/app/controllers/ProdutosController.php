<?php

class ProdutosController extends BaseController {

    public function index($slug = null)
    {
        View::share('categoria', 'produtos');
        View::share('projetos', Produto::ordenados()->get());

        if (!$slug) {

            return $this->view('frontend.projetos.index');

        } else {

            $selecionado = Produto::with('imagens')->slug($slug)->first();
            if (!$selecionado) App::abort('404');

            View::share('custom_layout', 'projeto');
            View::share('selecionado', $selecionado);
            return $this->view('frontend.projetos.show');

        }
    }

}
