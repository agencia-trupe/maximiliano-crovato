(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.setaPerfil = function() {
        var _this = $(this);

        if (_this.hasClass('active')) {
            $('.texto').fadeOut('slow', function() {
                $('.imagem').fadeIn('slow');
                _this.toggleClass('active');
            });
        } else {
            $('.imagem').fadeOut('slow', function() {
                $('.texto').fadeIn('slow');
                _this.toggleClass('active');
            });
        }
    };

    App.carouselProjetos = function() {
        var $container = $('.carousel');
        var index = $('#selecionado').index(),
            total = $container.find('a').length;

        if (!$container.length) return;
        console.log(total, index);
        index = (index > 0 ? index-1 : total-1);

        $container.cycle({
            fx: 'carousel',
            carouselVisible: (total < 6 ? total : 6),
            carouselVertical: true,
            startingSlide: index,
            next: '#carousel-prev',
            prev: '#carousel-next',
            slides: '>a',
            timeout: 0
        });
    };

    App.galeriaProjetos = function() {
        var $container = $('.galeria-slider');
        if (!$container.length) return;

        $container.cycle({
            slides: '>div',
            next: '#galeria-next',
            prev: '#galeria-prev'
        });
    };

    App.galeriaImprensa = function() {
        var $imprensa = $('.imprensa');
        if (!$imprensa.length) return;

        $('.fancybox').fancybox({
            padding: [10, 40, 10, 40],
            maxWidth: '95%',
            maxHeight: '85%'
        });

        $('.thumb').click(function(e) {
            var el, id = $(this).data('galeria');
            if(id){
                el = $('.fancybox[rel=galeria' + id + ']:eq(0)');
                e.preventDefault();
                el.click();
            }
        });
    };

    App.contatoEnvio = function(event) {
        event.preventDefault();

        var $formContato = $(this);
        var $formContatoSubmit = $formContato.find('input[type=submit]');
        var $formContatoResposta = $formContato.find('#form-resposta');

        $formContatoResposta.hide();

        $.post(BASE + '/contato', {

            nome     : $('#nome').val(),
            email    : $('#email').val(),
            telefone : $('#telefone').val(),
            mensagem : $('#mensagem').val()

        }, function(data) {

            if (data.status == 'success') $formContato[0].reset();
            $formContatoResposta.hide().text(data.message).fadeIn('slow');

        }, 'json');
    };

    App.gridBlog = function() {
        var $grid = $('.blog-thumbs');
        if (!$grid.length) return;

        $grid.waitForImages(function() {
            $grid.masonry({
                itemSelector: '.thumb',
                columnWidth: 254,
                gutter: 30
            });
        });
    };

    App.scrollBlog = function() {
        var $comentarios = $('#comentarios');
        if (!$comentarios.length) return;

        $('#topo').click(function(event) {
            event.preventDefault();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        });

        $('#comente').click(function(event) {
            event.preventDefault();
            $('html, body').animate({ scrollTop: $comentarios.offset().top - 30 }, 'slow');
        });
    };

    App.comentarioEnvio = function(event) {
        event.preventDefault();

        var $formComentario = $(this);
        var $formComentarioSubmit = $formComentario.find('input[type=submit]');
        var $formComentarioResposta = $formComentario.find('#form-resposta');

        $formComentarioResposta.hide();

        $.post(BASE + '/blog/comentario', {

            blog_id    : $('#blog_id').val(),
            autor      : $('#autor').val(),
            email      : $('#email').val(),
            comentario : $('#comentario').val()

        }, function(data) {

            if (data.status == 'success') $formComentario[0].reset();
            $formComentarioResposta.hide().text(data.message).fadeIn('slow');

        }, 'json');
    };

    App.init = function() {
        App.galeriaImprensa();
        App.carouselProjetos();
        App.galeriaProjetos();
        $('.perfil-seta').on('click', App.setaPerfil);
        $('#form-contato').on('submit', App.contatoEnvio);
        App.gridBlog();
        App.scrollBlog();
        $('#form-comentario').on('submit', App.comentarioEnvio);
    };

    $(document).ready(App.init);

}(window, document, jQuery));
